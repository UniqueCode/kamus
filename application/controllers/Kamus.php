<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . '/third_party/spout/src/Spout/Autoloader/autoload.php';

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

class Kamus extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Kamus_model');
        $this->load->library('form_validation');
        $this->id_pengguna = get_userdata('app_id_pengguna');
    }

    private function cekAkses($var = null)
    {
        $url = 'Kamus';
        return cek($this->id_pengguna, $url, $var);
    }

    public function index()

    {
        $akses = $this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url']  = base_url() . 'kamus?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'kamus?q=' . urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'kamus';
            $config['first_url'] = base_url() . 'kamus';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Kamus_model->total_rows($q);
        $kamus                      = $this->Kamus_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'kamus_data' => $kamus,
            'q'                   => $q,
            'pagination'          => $this->pagination->create_links(),
            'total_rows'          => $config['total_rows'],
            'start'               => $start,
            'title'               => 'Data Kamus',
            'create'              => 'Kamus/create',
            'import' => site_url('kamus/import'),
            'akses'               => $akses
        );
        $this->template->load('layout', 'kamus/Kamus_list', $data);
    }

    public function import()
    {
        $data = array(
            'action'  => site_url('kamus/import_action'),
            'kembali'              => 'kamus',
            'title'                    => 'Form Import'
        );
        $this->template->load('layout', 'kamus/Import_form', $data);
    }
    public function import_action()
    {
        $config['upload_path']   = 'temp/';
        $config['allowed_types']    = 'xlsx|xls'; //siapkan format file
        $config['file_name']        = 'doc'; //rename file yang diupload
        $this->upload->initialize($config);
        $this->load->library('upload', $config);
        if ($this->upload->do_upload('fileimport')) {
            $file   = $this->upload->data();

            $reader = ReaderFactory::create(Type::XLSX); //set Type file xlsx
            $reader->setShouldFormatDates(true);
            $reader->open('temp/' . $file['file_name']); //open file xlsx
            //looping pembacaat sheet dalam file
            foreach ($reader->getSheetIterator() as $sheet) {
                $numRow = 1;

                //siapkan variabel array kosong untuk menampung variabel array data
                $save   = array();

                //looping pembacaan row dalam sheet
                foreach ($sheet->getRowIterator() as $row) {
                    if ($numRow > 1) {
                        // echo substr(kelas($row[27]),0,1);
                        // echo $row[1];
                        // die();
                        // $x = $this->db->query("select count(nisn) nisn from ms_kamus where nisn='$row[1]'")->row();
                        $data = array(
                            'english'      => $row[0],
                            'indonesia'    => $row[1],
                            'kategori_eng' => $row[2],
                            'kategori_ina' => $row[3],
                            'date_insert' => date('Y-m-d H:i:s'),
                        );

                        //tambahkan array $data ke $save
                        // if ($x->nisn == 0) {
                            // var_dump($data);
                            // die();
                            array_push($save, $data);
                            $this->Kamus_model->insert($data);
                        // }
                    }

                    $numRow++;
                }
                unlink('temp/doc.xlsx');
                // die();

                //tutup spout reader
                $reader->close();

                set_flashdata('success', 'Import Data Berhasil.');
                redirect(site_url('kamus'));
                //hapus file yang sudah diupload
            }
        } else {
            echo "Error :" . $this->upload->display_errors(); //tampilkan pesan error jika file gagal diupload
            set_flashdata('warning', 'Data Gagal Disimpan.');
            redirect(site_url('kamus'));
        }
    }
    public function create()
    {
        $this->cekAkses('create');

        $data = array(
            'title'   => 'Tambah Data Kamus',
            'kembali' => 'Kamus',
            'action'  => site_url('kamus/create_action'),
            'id' => set_value('id'),
            'english' => set_value('english'),
            'indonesia' => set_value('indonesia'),
            'kategori_eng' => set_value('kategori_eng'),
            'kategori_ina' => set_value('kategori_ina'),
        );
        $this->template->load('layout', 'kamus/Kamus_form', $data);
    }

    public function create_action()
    {
        $this->cekAkses('create');

        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'english' => $this->input->post('english', TRUE),
                'indonesia' => $this->input->post('indonesia', TRUE),
                'kategori_eng' => $this->input->post('kategori_eng', TRUE),
                'kategori_ina' => $this->input->post('kategori_ina', TRUE),
                'date_insert' => date('Y-m-d H:i:s'),
            );

            $this->Kamus_model->insert($data);
            set_flashdata('success', 'Data telah di simpan.');
            redirect(site_url('kamus'));
        }
    }

    public function update($ide)
    {
        $this->cekAkses('update');
        $id = rapikan($ide);
        $row = $this->Kamus_model->get_by_id($id);

        if ($row) {
            $data = array(
                'title' => 'Edit data Kamus',
                'action' => site_url('kamus/update_action'),
                'kembali' => 'Kamus',
                'id' => set_value('id', $row->id),
                'english' => set_value('english', $row->english),
                'indonesia' => set_value('indonesia', $row->indonesia),
                'kategori_eng' => set_value('kategori_eng', $row->kategori_eng),
                'kategori_ina' => set_value('kategori_ina', $row->kategori_ina),
            );
            $this->template->load('layout', 'kamus/Kamus_form', $data);
        } else {
            set_flashdata('warning', 'Record Not Found.');
            redirect(site_url('kamus'));
        }
    }

    public function update_action()
    {
        $this->cekAkses('update');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
                'english' => $this->input->post('english', TRUE),
                'indonesia' => $this->input->post('indonesia', TRUE),
                'kategori_eng' => $this->input->post('kategori_eng', TRUE),
                'kategori_ina' => $this->input->post('kategori_ina', TRUE),
                'date_update' => date('Y-m-d H:i:s'),
            );

            $this->Kamus_model->update($this->input->post('id', TRUE), $data);
            set_flashdata('success', 'Update Record Success');
            redirect(site_url('kamus'));
        }
    }

    public function delete($ide)
    {
        $this->cekAkses('delete');
        $id = rapikan($ide);
        $row = $this->Kamus_model->get_by_id($id);

        if ($row) {
            $this->Kamus_model->delete($id);
            set_flashdata('success', 'Delete Record Success');
            redirect(site_url('kamus'));
        } else {
            set_flashdata('message', 'Record Not Found');
            redirect(site_url('kamus'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('english', 'english', 'trim|required');
        $this->form_validation->set_rules('indonesia', 'indonesia', 'trim|required');
        $this->form_validation->set_rules('kategori_eng', 'kategori eng', 'trim|required');
        $this->form_validation->set_rules('kategori_ina', 'kategori ina', 'trim|required');
        // $this->form_validation->set_rules('date_insert', 'date insert', 'trim|required');
        // $this->form_validation->set_rules('date_update', 'date update', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }
}

/* End of file Kamus.php */
/* Location: ./application/controllers/Kamus.php */
