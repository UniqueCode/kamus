<?php
/**
 * 
 */
class AndroidJson extends CI_Controller
{
	
	function index()
	{
		echo "sukses";
		
		
	}

	function HitungVocabulary()
	{
		$result = array();
		$query = $this->db->query("SELECT * FROM tb_vocabulary")->result();

		array_push($result, array(
			'jml_data'=>count($query)
		));

		echo json_encode(array("result"=>$result));
	}

	function SinkronVocabulary()
	{
		$result = array();
		$query = $this->db->query("SELECT * FROM tb_vocabulary")->result();

		foreach ($query as $a) {
			$du = '';
			if($a->date_update == ''){
				$du = '0000-00-00 00:00:00';
			}else{
				$du = $a->date_update;
			}
			array_push($result, array(
				'id'           => $a->id,
				'english'      => $a->english,
				'indonesia'    => $a->indonesia,
				'kategori_eng' => $a->kategori_eng,
				'kategori_ina' => $a->kategori_ina,
				'date_insert'  => $a->date_insert,
				'date_update'  => $du,
			));
		}
		

		echo json_encode(array("result"=>$result));
	}

	function CekPembaruan()
	{
		$result = array();

		$date_insert = $_POST['date_insert'];
		$date_update = $_POST['date_update'];

		$mdi = $this->db->query("SELECT date_insert AS max_di FROM tb_vocabulary ORDER BY date_insert DESC")->row();
		$mdu = $this->db->query("SELECT date_update AS max_du FROM tb_vocabulary ORDER BY date_update DESC")->row();

		$last_di = $mdi->max_di;
		$last_du = $mdu->max_du;

		$query_di = $this->db->query("SELECT * FROM tb_vocabulary WHERE date_insert BETWEEN '$date_insert' AND '$last_di' AND date_insert != '$date_insert' ")->result();

		if($last_du==''){
			$last_du = $date_update;
		}

		$query_du = $this->db->query("SELECT * FROM tb_vocabulary WHERE date_update BETWEEN '$date_update' AND '$last_du' AND date_update != '$date_update' ")->result();

		array_push($result, array(
			'jml_data'=>count($query_di) + count($query_du)
		));

		echo json_encode(array("result"=>$result));
	}

	function PembaruanVocabulary()
	{
		$result = array();

		$date_insert = $_GET['date_insert'];
		$date_update = $_GET['date_update'];

		$mdi = $this->db->query("SELECT date_insert AS max_di FROM tb_vocabulary ORDER BY date_insert DESC")->row();
		$mdu = $this->db->query("SELECT date_update AS max_du FROM tb_vocabulary ORDER BY date_update DESC")->row();


		$last_di = $mdi->max_di;
		$last_du = $mdu->max_du;

		$query_di = $this->db->query("SELECT * FROM tb_vocabulary WHERE date_insert BETWEEN '$date_insert' AND '$last_di' AND date_insert != '$date_insert' ")->result();

		foreach ($query_di as $a) {
			$du = '';
			if($a->date_update == ''){
				$du = '0000-00-00 00:00:00';
			}else{
				$du = $a->date_update;
			}
			array_push($result, array(
				'id'           => $a->id,
				'english'      => $a->english,
				'indonesia'    => $a->indonesia,
				'kategori_eng' => $a->kategori_eng,
				'kategori_ina' => $a->kategori_ina,
				'date_insert'  => $a->date_insert,
				'date_update'  => $du,
				'status'       => 'i',
			));
		}

		if($last_du==''){
			$last_du = $date_update;
		}

		$query_du = $this->db->query("SELECT * FROM tb_vocabulary WHERE date_update BETWEEN '$date_update' AND '$last_du' AND date_update != '$date_update' ")->result();

		foreach ($query_du as $b) {
			$du = '';
			if($b->date_update == ''){
				$du = '0000-00-00 00:00:00';
			}else{
				$du = $b->date_update;
			}
			array_push($result, array(
				'id'           => $b->id,
				'english'      => $b->english,
				'indonesia'    => $b->indonesia,
				'kategori_eng' => $b->kategori_eng,
				'kategori_ina' => $b->kategori_ina,
				'date_insert'  => $b->date_insert,
				'date_update'  => $du,
				'status'       => 'u',
			));
		}
		

		echo json_encode(array("result"=>$result));
	}

}
?>