<div class="row">
<div class="col-md-12">
  <div class="box">
    <!-- /.box-header -->
    <div class="box-body">
    <div class="row">
          <form action="<?php echo site_url('pengguna'); ?>"  method="get">
              <div class="col-md-3">
                  <div class="form-group">
                      <label>Pencarian</label>
                      <input type="text" class="form-control"  placeholder="Pencarian" name="q" value="<?= $q?>">
                  </div>
              </div>
              <div class="col-md-3" style="padding-top:25px">
                  <div class="form-group">
                      <label ></label>
                      <?php  if ($q <> '') {   ?>
                        <a href="<?php echo site_url('pengguna'); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</a>
                      <?php   } ?>

                      <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i> Cari</button>
                  </div>
              </div>
          </form>
      </div>
      <table class="table table-bordered">
      <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Lengkap</th>
                                <th>No Telepon</th>
                                <th>Email</th>
                                <th>Username</th>
                                <th>Role</th>
                                <th></th>
                                </tr>
        </thead>
        <tbody>
                            <?php
                            foreach ($pengguna_data as $pengguna)
                            {
                                ?>
                                <tr>
                        			<td align="center" width="80px"><?php echo ++$start ?></td>
                        			<td><?php echo $pengguna->nama_lengkap ?></td>
                        			<td><?php echo $pengguna->no_telepon ?></td>
                        			<td><?php echo $pengguna->email ?></td>
                        			<td><?php echo $pengguna->username ?></td>
                                    <td><?php echo $pengguna->role?></td>
                        			<td style="text-align:center" width="100px">
                        				<?php if($akses['is_update']==1){
                        				echo anchor(site_url('pengguna/update/'.acak($pengguna->id_inc)),'<i class="fa fa-edit"></i>','class="badge badge-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit data"'); }
                        				 if($akses['is_delete']==1){echo anchor(site_url('pengguna/delete/'.acak($pengguna->id_inc)),'<i class="fa fa-trash"></i>','class="badge badge-danger" onclick="javasciprt: return confirm(\'Apakah anda yakin? data yang telah di hapus tidak dapat di kembalikan!\')" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus data"'); }
                        				?>
                        			</td>
                        		</tr>
                                <?php
                            }
                            ?>
              </tbody>
      </table>
    </div>
    <div class="box-footer clearfix">
      <span class="pull-left">
      <button type="button" class="btn btn-block btn-success btn-sm">Record : <?php echo $total_rows ?></button>
      </span>
            <?php echo $pagination ?>
      </div>
  </div>
</div>
</div>