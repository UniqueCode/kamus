<div class="row">
 <div class="col-md-12">
   <div class="box">
     <!-- /.box-header -->
     <div class="box-body">
          <form method="post" action="<?php echo base_url().'role/prosessettingrole'?>">
                    <button class="btn float-right btn-sm btn-primary"><i class="mdi mdi-cloud-done"></i> Submit</button>
                    <input type="hidden" name="kode_group"  value="<?php echo $id_inc;?>">
       <table class="table table-bordered">
       <thead>
          <tr>
            <th colspan="4">Akses</th>
            <th rowspan="2">menu</th>
            <th rowspan="2">Parent</th>
          </tr>
          <tr>
            <td width="3%">Read</td>
            <td width="3%">Create</td>
            <td width="3%">Update</td>
            <td width="3%">Delete</td>
          </tr>
        </thead>
        <tbody>
              <?php
              foreach($role as $row){?>
            <tr>
              <td align="center"> <input <?php if($row['status']==1){ echo "checked";}?> type="checkbox" name="role[]" value="<?php echo $row['kode_role']; ?>"></td>
              <td align="center"> <input <?php if($row['is_create']==1){ echo "checked";}?> type="checkbox" name="create[<?php echo $row['kode_role']; ?>]" value="1"></td>
              <td align="center"> <input <?php if($row['is_update']==1){ echo "checked";}?> type="checkbox" name="update[<?php echo $row['kode_role']; ?>]" value="1"></td>
              <td align="center"> <input <?php if($row['is_delete']==1){ echo "checked";}?> type="checkbox" name="delete[<?php echo $row['kode_role']; ?>]" value="1"></td>
              <td><?php echo ucwords($row['nama_menu']);?></td>
              <td><?php echo ucwords($row['parent']);?></td>
            </tr>
          <?php }?>
        </tbody>
       </table>
                </form>
     </div>
     <!-- /.box-body -->
   </div>
   <!-- /.box -->
 </div>
</div>