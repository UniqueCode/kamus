
<div class="row">
        <div class="col-md-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
              <thead>
                    <tr>
                        <th width="15px">#</th>
                        <th>Nama Role</th>
                        <th width="150px">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no=1; foreach($data as $rk){ ?>
                        <tr>
                            <td><?= $no ?></td>
                            <td><?= $rk->nama_role ?></td>
                            <td align="center">
                                <div class="btn-group">
                                    <?php if($akses['is_update']=='1'){
                                         echo anchor('role/setting?q='.acak($rk->id_inc),'<i class="fa fa-gear"></i>','class="btn btn-sm btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Atur hak akses"');
                                        echo anchor('role/edit?q='.acak($rk->id_inc),'<i class="fa  fa-edit "></i>','class="btn btn-sm btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit data"');
                                     }?>
                                    <?php if($akses['is_delete']=='1'){
                                        // mdi mdi-delete
                                        echo anchor('role/delete?q='.acak($rk->id_inc),'<i class="fa  fa-trash-o"></i>','class="btn btn-sm btn-danger btn-xs" onclick="return confirm(\'Apakah anda yakin?\')" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus data"');
                                    }?>
                                </div>
                            </td>
                        </tr>
                    <?php $no++; } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>