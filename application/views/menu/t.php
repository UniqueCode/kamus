<?php
// $string=\"\";

$string="<div class=\"row\">
  <div class=\"col-md-12\">
    <div class=\"box\">
      <!-- /.box-header -->
      <div class=\"box-body\">
      <div class=\"row\">
            <form action=\"<?php echo site_url('$c_url'); ?>\"  method=\"get\">
                <div class=\"col-md-3\">
                    <div class=\"form-group\">
                        <label>Pencarian</label>
                        <input type=\"text\" class=\"form-control\"  placeholder=\"Pencarian\" name=\"q\" value=\"<?= $q?>\">
                    </div>
                </div>
                <div class=\"col-md-3\" style=\"padding-top:25px\">
                    <div class=\"form-group\">
                        <label ></label>
                        <?php  if (\$q <> '') {   ?>
                          <a href=\"<?php echo site_url('$c_url'); ?>\" class=\"btn btn-warning\"><i class=\"fa fa-refresh\"></i> Reset</a>
                        <?php   } ?>

                        <button class=\"btn btn-primary\" type=\"submit\"><i class=\"fa fa-search\"></i> Cari</button>
                    </div>
                </div>
            </form>
        </div>
        <table class=\"table table-bordered\">
        <thead>
            <tr>
                <th>No</th>";
                foreach ($non_pk as $row) {
                    $string .= "\n\t\t<th>" . label($row['column_name']) . "</th>";
                }
                $string .= "\n\t\t<th></th>
            </tr>
          </thead>
          <tbody>";

          $string .= "<?php
          foreach ($" . $c_url . "_data as \$$c_url)
          {
              ?>
              <tr>";
              $string .= "\n\t\t\t<td align=\"center\" width=\"80px\"><?php echo ++\$start ?></td>";
              foreach ($non_pk as $row) {
                $string .= "\n\t\t\t<td><?php echo $" . $c_url ."->". $row['column_name'] . " ?></td>";
            }
            $string .= "\n\t\t\t<td style=\"text-align:center\" width=\"100px\">"
            . "\n\t\t\t\t<?php if(\$akses['is_update']==1){"
            . "\n\t\t\t\techo anchor(site_url('".$c_url."/update/'.acak($".$c_url."->".$pk.")),'<i class=\"fa fa-edit\"></i>','class=\"badge badge-info\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"\" data-original-title=\"Edit data\"'); }"
            . "\n\t\t\t\t if(\$akses['is_delete']==1){echo anchor(site_url('".$c_url."/delete/'.acak($".$c_url."->".$pk.")),'<i class=\"fa fa-trash\"></i>','class=\"badge badge-danger\" onclick=\"javasciprt: return confirm(\\'Apakah anda yakin? data yang telah di hapus tidak dapat di kembalikan!\\')\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"\" data-original-title=\"Hapus data\"'); }"
            . "\n\t\t\t\t?>"
            . "\n\t\t\t</td>";

    $string .=  "\n\t\t</tr>
                    <?php
                }
                ?>
                </tbody>
        </table>
      </div>
      <div class=\"box-footer clearfix\">
        <span class=\"pull-left\">
        <button type=\"button\" class=\"btn btn-block btn-success btn-sm\">Record : <?php echo \$total_rows ?></button>
        </span>
              <?php echo \$pagination ?>
        </div>
    </div>
  </div>
</div>";
?>