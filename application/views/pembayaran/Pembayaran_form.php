<div class="row">
        <div class="col-sm-12">
            <div class="box">
                <div class="box-body">
                <form action="<?php echo $action; ?>" method="post"  role="form" id="frm">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Siswa</h3>
                    </div>
                    <div class="row">
                        <div class="col-sm-3" >
                            <div class="form-group ">
                                <label for="kd_tagihan" >NISN<sup id="sup" style="color:red;display: none;">*</sup></label>
                                <div class="input-group carinip" style="cursor: zoom-in;">
                                    <span class="input-group-addon"><i class="fa fa-search "></i></span>
                                    <input readonly="" style="cursor: zoom-in;" type="text" id="nisn" class="carinip form-control" placeholder="NISN" value="<?php echo $nisn; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 ssss" >
                            <div class="form-group">
                                <label for="nominal">Nama  <sup style="color:red;">*</sup></label>
                                <input readonly type="text" value="<?php echo $nama; ?>" class="form-control" name="nama" id="nama" placeholder="Nama">
                            </div>
                        </div>
                        <div class="col-sm-3 ssss" >
                            <div class="form-group">
                                <label for="nominal">Kelas  <sup style="color:red;">*</sup></label>
                                <input readonly type="text" value="<?php echo $kelas; ?>" class="form-control" name="kelas" id="kelas" placeholder="Kelas">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="nominal">Tanggal  <sup style="color:red;">*</sup></label>
                                <input type="text" value="<?php echo $tanggal_bayar; ?>" class="form-control datepicker" name="tanggal_bayar" id="tanggal_bayar" placeholder="Tanggal">
                            </div>
                        </div>
                    </div>
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Tagihan</h3>
                    </div>
                    <div id="tagihan_list">

                    </div>
                    <span id="riwayat">

                    <div class="box-header with-border">
                        <h3 class="box-title">Riwayat Pembayaran</h3>
                    </div>
                        <span id="tbl_riwayat"></span>
                    </span>
                    <div class="row">

                        <!-- <div class="col-sm-3">
                            <div class="form-group">
                                <label for="nominal">Jumlah Bayar  <sup style="color:red;">*</sup></label>
                                <input type="text" class="form-control"  placeholder="Jumlah Bayar" id="jml_bayar" readonly>
                            </div>
                        </div> -->
                    </div>
                    <!-- <input type="hidden" name="kd_piutang" id="kd_piutang" value="<?php echo $kd_piutang; ?>" /> -->
                    <input type="hidden" name="id" value="<?php echo $id; ?>" />
	              <div class="box-footer">
              <button type="submit" class="btn btn-primary btn-sm">Submit</button>
              <!-- <?= anchor('pembayaran','<i class="fa fa-close"></i> Batal','class="btn btn-sm btn-warning"');?> -->
            </div>

                </form>
            </div>
        </div>
    </div>
    </div>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:800px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">List Siswa</h4>
            </div>
            <div class="modal-body">
                <table id="example"  class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th style ="width:70px " >NISN</th>
                            <th style ="width:50px " >Induk</th>
                            <th style ="width:390px " >Nama</th>
                            <th style ="width:60px " >Kelas</th>
                            <th style ="width:70px " >Angkatan</th>
                        </tr>
                    </thead>
                    <tbody>
                          <?php
                    foreach ($mahasiswa as $mahasiswa)
                    {
                        ?>
                        <tr style="cursor: pointer;" class="pilih"  data-nisn="<?php echo $mahasiswa->nisn ?>" data-nama="<?php echo $mahasiswa->nama ?>"
                        data-induk="<?php echo $mahasiswa->induk ?>" data-kelas="<?php echo kelas($mahasiswa->kd_kelas)?>" data-angkatan="<?php echo substr(kelas($mahasiswa->kd_kelas),0,1) ?>">
                            <td><?php echo $mahasiswa->nisn ?></td>
                            <td><?php echo $mahasiswa->induk ?></td>
                            <td><?php echo $mahasiswa->nama ?></td>
                            <td><?php echo kelas($mahasiswa->kd_kelas) ?></td>
                            <td><?php echo substr(kelas($mahasiswa->kd_kelas),0,1) ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th id="tf_nisn">NISN</th>
                            <th id="tf_induk">Induk</th>
                            <th id="tf_nama">Nama</th>
                            <th id="tf_kelas">Kelas</th>
                            <th id="tf_angkatan">Angkatan</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>