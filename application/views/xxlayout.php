<!doctype html>
<html lang="en">

<!-- Mirrored from coderthemes.com/highdmin/horizontal/page-starter.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 06 Jan 2019 08:32:45 GMT -->
<head>
        <meta charset="utf-8" />
        <title><?= $title ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="<?= base_url() ?>highdmin/horizontal/assets/images/favicon.ico">


        <!-- App css -->
        <link href="<?= base_url() ?>highdmin/horizontal/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <link href="<?= base_url() ?>highdmin/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">



        <link href="<?= base_url() ?>highdmin/horizontal/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>highdmin/horizontal/assets/css/style.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?= base_url() ?>highdmin/plugins/tooltipster/tooltipster.bundle.min.css">
        <link href="<?= base_url() ?>highdmin/plugins/jquery-toastr/jquery.toast.min.css" rel="stylesheet" />
        <link href="<?= base_url() ?>highdmin/plugins/select2/select2.min.css" rel="stylesheet" type="text/css" />




         <!-- DataTables -->
        <link href="<?= base_url() ?>highdmin/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>highdmin/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="<?= base_url() ?>highdmin/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- datepicker -->

        <!-- Multi Item Selection examples -->
        <link href="<?= base_url() ?>highdmin/plugins/datatables/select.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <script src="<?= base_url() ?>highdmin/horizontal/assets/js/modernizr.min.js"></script>

        <style type="text/css">
            .datepicker {
  z-index:10001 !important;
}
        </style>
    </head>

    <body>

        <!-- Navigation Bar-->
        <header id="topnav">
            <div class="topbar-main">
                <div class="container-fluid">

                    <!-- Logo container-->
                    <div class="logo">
                        <!-- Text Logo -->
                        <!-- <a href="index.html" class="logo">
                            <span class="logo-small"><i class="mdi mdi-radar"></i></span>
                            <span class="logo-large"><i class="mdi mdi-radar"></i> Highdmin</span>
                        </a> -->
                        <!-- Image Logo -->
                        <a href="index.html" class="logo">
                        <p style="margin-bottom:0px;padding-top:4px; font-size:10px"><img src="<?= base_url() ?>assets/logo.jpeg" alt="" height="50" class="logo-large"> Sistem Pencatatan dan Pelaporan Terpadu Puskesmas UPTD Puskesmas Mrican Kota Kediri<img src="<?= base_url() ?>assets/2.jpeg" alt="" height="50" class="logo-large"> </p>
                        </a>

                    </div>
                    <!-- End Logo container-->


                    <div class="menu-extras topbar-custom">

                        <ul class="list-unstyled topbar-right-menu float-right mb-0">

                            <li class="menu-item">
                                <!-- Mobile menu toggle-->
                                <a class="navbar-toggle nav-link">
                                    <div class="lines">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                </a>
                                <!-- End mobile menu toggle-->
                            </li>
                            <li class="dropdown notification-list">
                                <a class="nav-link dropdown-toggle waves-effect nav-user" data-toggle="dropdown" href="#" role="button"
                                   aria-haspopup="false" aria-expanded="false">
                                    <img src="<?= base_url() ?>highdmin/horizontal/assets/images/users/avatar-1.jpg" alt="user" class="rounded-circle"> <span class="ml-1 pro-user-name"><?php echo $this->session->userdata('app_nama'); ?><i class="mdi mdi-chevron-down"></i> </span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown">

                                    <!-- item-->
                                    <a href="<?= base_url().'auth/logout '?>" class="dropdown-item notify-item">
                                        <i class="fi-power"></i> <span>Logout</span>
                                    </a>

                                </div>
                            </li>
                        </ul>
                    </div>
                    <!-- end menu-extras -->

                    <div class="clearfix"></div>

                </div> <!-- end container -->
            </div>
            <!-- end topbar-main -->

            <div class="navbar-custom">
                <div class="container-fluid">
                    <div id="navigation">
                        <!-- Navigation Menu-->
                        <?php $this->load->view('menu'); ?>
                        <!-- End navigation menu -->
                    </div> <!-- end #navigation -->
                </div> <!-- end container -->
            </div> <!-- end navbar-custom -->
        </header>
        <!-- End Navigation Bar-->


        <div class="wrapper">
            <div class="container-fluid">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                             <div class="btn-group float-right">
                                  <?php
                                    if(isset($akses)){
                                        if($akses['is_create']==1){
                                            echo anchor($create,'<i class="mdi mdi-note-plus"></i> Tambah','class="btn btn-success"');
                                              }
                                          }

                                          if(isset($status)&&$this->session->userdata('app_role')==7){
                                              if($status!=6){
                                                echo anchor($link,'<i class="fa fa-check"></i>'.$label,'class="btn btn-success" onclick="javasciprt: return confirm(\'Apakah anda yakin?!\')"');
                                              }
                                            }
                                          if(isset($kembali)){
                                              echo anchor($kembali,'<i class="mdi mdi-backburger"></i> kembali','class="btn btn-info"');
                                          }
                                  ?>
                            </div>
                            <h4 class="page-title"><?= $title ?></h4>
                        </div>
                    </div>
                </div>
                <!-- Page-Title -->
               <?= $contents ?>
                <!-- end page title end breadcrumb -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->


        <!-- Footer -->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        2019 © Sri Nuraini


                    </div>
                </div>
            </div>
        </footer>
        <!-- End Footer -->


        <!-- jQuery  -->
        <script src="<?= base_url() ?>highdmin/horizontal/assets/js/jquery.min.js"></script>
        <script src="<?= base_url() ?>highdmin/horizontal/assets/js/bootstrap.bundle.min.js"></script>
        <script src="<?= base_url() ?>highdmin/horizontal/assets/js/waves.js"></script>
        <script src="<?= base_url() ?>highdmin/horizontal/assets/js/jquery.slimscroll.js"></script>
        <script src="<?= base_url() ?>highdmin/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>


        <!-- App js -->
        <script src="<?= base_url() ?>highdmin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
        <script src="<?= base_url() ?>highdmin/plugins/jquery-toastr/jquery.toast.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>highdmin/horizontal/assets/js/jquery.core.js"></script>
        <script src="<?= base_url() ?>highdmin/horizontal/assets/js/jquery.app.js"></script>
        <script src="<?= base_url() ?>highdmin/plugins/select2/select2.min.js"></script>



        <!-- Required datatable js -->
        <script src="<?= base_url() ?>highdmin/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?= base_url() ?>highdmin/plugins/datatables/dataTables.bootstrap4.min.js"></script>

        <?php if(isset($script)){ $this->load->view($script);  }?>
        <script type="text/javascript">


            $( document ).ready(function() {
                $('.datepicker').datepicker({
                      format: 'dd-mm-yyyy',
                      autoclose:true
    // startDate: '-3d'
                    });
                // datatables
                $('.datatables').DataTable();
                 // Select2
                $(".select2").select2();
                $(".select2-limiting").select2({
                    maximumSelectionLength: 2
                });

                <?php  if(!empty(get_flashdata('icon'))){?>

                $.toast({
                     heading  : '<?= get_flashdata("head")?>',
                     text     : '<?= get_flashdata("msg")?>',
                     position : 'top-right',
                     loaderBg : '#5ba035',
                     icon     : '<?= get_flashdata("icon")?>',
                     hideAfter: 3000,
                });
                <?php } ?>
            });

            function formatangka(objek) {
               a = objek.value;
               b = a.replace(/[^\d]/g,"");
               c = "";
               panjang = b.length;
               j = 0;
               for (i = panjang; i > 0; i--) {
                 j = j + 1;
                 if (((j % 3) == 1) && (j != 1)) {
                   c = b.substr(i-1,1) + "." + c;
                 } else {
                   c = b.substr(i-1,1) + c;
                 }
               }
               objek.value = c;
            };
        </script>

    </body>

<!-- Mirrored from coderthemes.com/highdmin/horizontal/page-starter.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 06 Jan 2019 08:32:45 GMT -->
</html>