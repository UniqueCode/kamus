<div class="row">
        <div class="col-sm-12">
            <div class="box">
                <div class="box-body">
                <form action="<?php echo $action; ?>" method="post"  role="form">
                <div class="box-body">


                    <div class="form-group">
                    <label for="english">English  <sup style="color:red;">*</sup></label>
                    <textarea class="form-control" rows="3" name="english" id="english" placeholder="English"><?php echo $english; ?></textarea>
                    <?php echo form_error('english') ?>
                </div>

                    <div class="form-group">
                    <label for="indonesia">Indonesia  <sup style="color:red;">*</sup></label>
                    <textarea class="form-control" rows="3" name="indonesia" id="indonesia" placeholder="Indonesia"><?php echo $indonesia; ?></textarea>
                    <?php echo form_error('indonesia') ?>
                </div>

                    <div class="form-group">
                    <label for="kategori_eng">Kategori Eng  <sup style="color:red;">*</sup></label>
                    <textarea class="form-control" rows="3" name="kategori_eng" id="kategori_eng" placeholder="Kategori Eng"><?php echo $kategori_eng; ?></textarea>
                    <?php echo form_error('kategori_eng') ?>
                </div>

                    <div class="form-group">
                    <label for="kategori_ina">Kategori Ina  <sup style="color:red;">*</sup></label>
                    <textarea class="form-control" rows="3" name="kategori_ina" id="kategori_ina" placeholder="Kategori Ina"><?php echo $kategori_ina; ?></textarea>
                    <?php echo form_error('kategori_ina') ?>
                </div>
	    </div><input type="hidden" name="id" value="<?php echo $id; ?>" />
	              <div class="box-footer">
              <button type="submit" class="btn btn-primary btn-sm">Submit</button>
              <?= anchor('kamus','<i class="fa fa-close"></i> Batal','class="btn btn-sm btn-warning"');?>
            </div>

                </form>
            </div>
        </div>
    </div>
    </div>